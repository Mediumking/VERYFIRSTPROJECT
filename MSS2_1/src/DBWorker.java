import java.sql.*;


public class DBWorker {
    private static final String url = "jdbc:mysql://localhost:3306/test";
    private static final String user = "root";
    private static final String password = "root";
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;
    public DBWorker(){
        try{
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
        }
        catch(java.sql.SQLException e){
            e.printStackTrace();
        }
    }
    public void changedata (String query) {
        try {
            stmt.executeUpdate(query);
        }
        catch(java.sql.SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void selectdata (String query)
    {
        try{
            rs = stmt.executeQuery(query);
        }
        catch(java.sql.SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void adddata(String query)
    {
        changedata(query);
    }
    public void checkdata(String query)
    {
        selectdata(query);

    }
}
